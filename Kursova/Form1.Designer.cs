﻿namespace Kursova
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button2 = new System.Windows.Forms.Button();
            this.db_kursDataSet = new Kursova.db_kursDataSet();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.kodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pibDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posadaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.okladDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableinfBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.tableinfBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.kodDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vidrobdenDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.misyacDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rikDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kodspivrobDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.premDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kilklikDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumapodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.narahuvannyaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zarplataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tablezpBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fKtablezptableinfBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_infTableAdapter = new Kursova.db_kursDataSetTableAdapters.table_infTableAdapter();
            this.table_zpTableAdapter = new Kursova.db_kursDataSetTableAdapters.table_zpTableAdapter();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.db_kursDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableinfBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableinfBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablezpBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKtablezptableinfBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(665, 433);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 32);
            this.button2.TabIndex = 3;
            this.button2.Text = "Вихід";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // db_kursDataSet
            // 
            this.db_kursDataSet.DataSetName = "db_kursDataSet";
            this.db_kursDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kodDataGridViewTextBoxColumn,
            this.pibDataGridViewTextBoxColumn,
            this.telDataGridViewTextBoxColumn,
            this.adressDataGridViewTextBoxColumn,
            this.posadaDataGridViewTextBoxColumn,
            this.okladDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tableinfBindingSource1;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(575, 150);
            this.dataGridView1.TabIndex = 4;
            // 
            // kodDataGridViewTextBoxColumn
            // 
            this.kodDataGridViewTextBoxColumn.DataPropertyName = "kod";
            this.kodDataGridViewTextBoxColumn.HeaderText = "Код";
            this.kodDataGridViewTextBoxColumn.Name = "kodDataGridViewTextBoxColumn";
            this.kodDataGridViewTextBoxColumn.ReadOnly = true;
            this.kodDataGridViewTextBoxColumn.Width = 30;
            // 
            // pibDataGridViewTextBoxColumn
            // 
            this.pibDataGridViewTextBoxColumn.DataPropertyName = "pib";
            this.pibDataGridViewTextBoxColumn.HeaderText = "ПІБ";
            this.pibDataGridViewTextBoxColumn.Name = "pibDataGridViewTextBoxColumn";
            this.pibDataGridViewTextBoxColumn.ReadOnly = true;
            this.pibDataGridViewTextBoxColumn.Width = 140;
            // 
            // telDataGridViewTextBoxColumn
            // 
            this.telDataGridViewTextBoxColumn.DataPropertyName = "tel";
            this.telDataGridViewTextBoxColumn.HeaderText = "Телефон";
            this.telDataGridViewTextBoxColumn.Name = "telDataGridViewTextBoxColumn";
            this.telDataGridViewTextBoxColumn.ReadOnly = true;
            this.telDataGridViewTextBoxColumn.Width = 60;
            // 
            // adressDataGridViewTextBoxColumn
            // 
            this.adressDataGridViewTextBoxColumn.DataPropertyName = "adress";
            this.adressDataGridViewTextBoxColumn.HeaderText = "Адреса";
            this.adressDataGridViewTextBoxColumn.Name = "adressDataGridViewTextBoxColumn";
            this.adressDataGridViewTextBoxColumn.ReadOnly = true;
            this.adressDataGridViewTextBoxColumn.Width = 140;
            // 
            // posadaDataGridViewTextBoxColumn
            // 
            this.posadaDataGridViewTextBoxColumn.DataPropertyName = "posada";
            this.posadaDataGridViewTextBoxColumn.HeaderText = "Посада";
            this.posadaDataGridViewTextBoxColumn.Name = "posadaDataGridViewTextBoxColumn";
            this.posadaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // okladDataGridViewTextBoxColumn
            // 
            this.okladDataGridViewTextBoxColumn.DataPropertyName = "oklad";
            this.okladDataGridViewTextBoxColumn.HeaderText = "Оклад";
            this.okladDataGridViewTextBoxColumn.Name = "okladDataGridViewTextBoxColumn";
            this.okladDataGridViewTextBoxColumn.ReadOnly = true;
            this.okladDataGridViewTextBoxColumn.Width = 50;
            // 
            // tableinfBindingSource1
            // 
            this.tableinfBindingSource1.DataMember = "table_inf";
            this.tableinfBindingSource1.DataSource = this.bindingSource1;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = this.db_kursDataSet;
            this.bindingSource1.Position = 0;
            // 
            // tableinfBindingSource
            // 
            this.tableinfBindingSource.DataMember = "table_inf";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kodDataGridViewTextBoxColumn1,
            this.vidrobdenDataGridViewTextBoxColumn,
            this.misyacDataGridViewTextBoxColumn,
            this.rikDataGridViewTextBoxColumn,
            this.kodspivrobDataGridViewTextBoxColumn,
            this.premDataGridViewTextBoxColumn,
            this.kilklikDataGridViewTextBoxColumn,
            this.sumapodDataGridViewTextBoxColumn,
            this.narahuvannyaDataGridViewTextBoxColumn,
            this.zarplataDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.tablezpBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(12, 190);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(647, 275);
            this.dataGridView2.TabIndex = 5;
            // 
            // kodDataGridViewTextBoxColumn1
            // 
            this.kodDataGridViewTextBoxColumn1.DataPropertyName = "kod";
            this.kodDataGridViewTextBoxColumn1.HeaderText = "Код";
            this.kodDataGridViewTextBoxColumn1.Name = "kodDataGridViewTextBoxColumn1";
            this.kodDataGridViewTextBoxColumn1.ReadOnly = true;
            this.kodDataGridViewTextBoxColumn1.Width = 30;
            // 
            // vidrobdenDataGridViewTextBoxColumn
            // 
            this.vidrobdenDataGridViewTextBoxColumn.DataPropertyName = "vidrob_den";
            this.vidrobdenDataGridViewTextBoxColumn.HeaderText = "Відпрацьовано днів";
            this.vidrobdenDataGridViewTextBoxColumn.Name = "vidrobdenDataGridViewTextBoxColumn";
            this.vidrobdenDataGridViewTextBoxColumn.ReadOnly = true;
            this.vidrobdenDataGridViewTextBoxColumn.Width = 85;
            // 
            // misyacDataGridViewTextBoxColumn
            // 
            this.misyacDataGridViewTextBoxColumn.DataPropertyName = "misyac";
            this.misyacDataGridViewTextBoxColumn.HeaderText = "Місяць";
            this.misyacDataGridViewTextBoxColumn.Name = "misyacDataGridViewTextBoxColumn";
            this.misyacDataGridViewTextBoxColumn.ReadOnly = true;
            this.misyacDataGridViewTextBoxColumn.Width = 45;
            // 
            // rikDataGridViewTextBoxColumn
            // 
            this.rikDataGridViewTextBoxColumn.DataPropertyName = "rik";
            this.rikDataGridViewTextBoxColumn.HeaderText = "Рік";
            this.rikDataGridViewTextBoxColumn.Name = "rikDataGridViewTextBoxColumn";
            this.rikDataGridViewTextBoxColumn.ReadOnly = true;
            this.rikDataGridViewTextBoxColumn.Width = 40;
            // 
            // kodspivrobDataGridViewTextBoxColumn
            // 
            this.kodspivrobDataGridViewTextBoxColumn.DataPropertyName = "kod_spivrob";
            this.kodspivrobDataGridViewTextBoxColumn.HeaderText = "Код співробітника";
            this.kodspivrobDataGridViewTextBoxColumn.Name = "kodspivrobDataGridViewTextBoxColumn";
            this.kodspivrobDataGridViewTextBoxColumn.ReadOnly = true;
            this.kodspivrobDataGridViewTextBoxColumn.Width = 80;
            // 
            // premDataGridViewTextBoxColumn
            // 
            this.premDataGridViewTextBoxColumn.DataPropertyName = "prem";
            this.premDataGridViewTextBoxColumn.HeaderText = "Премія";
            this.premDataGridViewTextBoxColumn.Name = "premDataGridViewTextBoxColumn";
            this.premDataGridViewTextBoxColumn.ReadOnly = true;
            this.premDataGridViewTextBoxColumn.Width = 45;
            // 
            // kilklikDataGridViewTextBoxColumn
            // 
            this.kilklikDataGridViewTextBoxColumn.DataPropertyName = "kilk_lik";
            this.kilklikDataGridViewTextBoxColumn.HeaderText = "Лікарняні дні";
            this.kilklikDataGridViewTextBoxColumn.Name = "kilklikDataGridViewTextBoxColumn";
            this.kilklikDataGridViewTextBoxColumn.ReadOnly = true;
            this.kilklikDataGridViewTextBoxColumn.Width = 65;
            // 
            // sumapodDataGridViewTextBoxColumn
            // 
            this.sumapodDataGridViewTextBoxColumn.DataPropertyName = "suma_pod";
            this.sumapodDataGridViewTextBoxColumn.HeaderText = "Сума податків";
            this.sumapodDataGridViewTextBoxColumn.Name = "sumapodDataGridViewTextBoxColumn";
            this.sumapodDataGridViewTextBoxColumn.ReadOnly = true;
            this.sumapodDataGridViewTextBoxColumn.Width = 60;
            // 
            // narahuvannyaDataGridViewTextBoxColumn
            // 
            this.narahuvannyaDataGridViewTextBoxColumn.DataPropertyName = "narahuvannya";
            this.narahuvannyaDataGridViewTextBoxColumn.HeaderText = "Нарахування";
            this.narahuvannyaDataGridViewTextBoxColumn.Name = "narahuvannyaDataGridViewTextBoxColumn";
            this.narahuvannyaDataGridViewTextBoxColumn.ReadOnly = true;
            this.narahuvannyaDataGridViewTextBoxColumn.Width = 80;
            // 
            // zarplataDataGridViewTextBoxColumn
            // 
            this.zarplataDataGridViewTextBoxColumn.DataPropertyName = "zarplata";
            this.zarplataDataGridViewTextBoxColumn.HeaderText = "Заробітна плата";
            this.zarplataDataGridViewTextBoxColumn.Name = "zarplataDataGridViewTextBoxColumn";
            this.zarplataDataGridViewTextBoxColumn.ReadOnly = true;
            this.zarplataDataGridViewTextBoxColumn.Width = 60;
            // 
            // tablezpBindingSource
            // 
            this.tablezpBindingSource.DataMember = "table_zp";
            this.tablezpBindingSource.DataSource = this.bindingSource1;
            // 
            // fKtablezptableinfBindingSource
            // 
            this.fKtablezptableinfBindingSource.DataSource = this.tableinfBindingSource;
            // 
            // table_infTableAdapter
            // 
            this.table_infTableAdapter.ClearBeforeFill = true;
            // 
            // table_zpTableAdapter
            // 
            this.table_zpTableAdapter.ClearBeforeFill = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(593, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 39);
            this.button3.TabIndex = 6;
            this.button3.Text = "Додати працівника";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Location = new System.Drawing.Point(593, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(471, 172);
            this.panel1.TabIndex = 7;
            this.panel1.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(143, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Код нового працівника";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(299, 148);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 21);
            this.button5.TabIndex = 19;
            this.button5.Text = "Додати";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(90, 118);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(364, 20);
            this.textBox4.TabIndex = 18;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(90, 92);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(364, 20);
            this.textBox3.TabIndex = 17;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(90, 68);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(365, 20);
            this.textBox2.TabIndex = 16;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Директор",
            "Головний бухгалтер",
            "Бухгалтер",
            "Менеджер",
            "Програміст",
            "Юніор"});
            this.comboBox1.Location = new System.Drawing.Point(90, 41);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(365, 21);
            this.comboBox1.TabIndex = 15;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(90, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(365, 20);
            this.textBox1.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(14, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Оклад";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(14, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Посада";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Адреса";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Телефон";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(14, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "ПІБ";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(380, 148);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 21);
            this.button4.TabIndex = 8;
            this.button4.Text = "Відмінити";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(674, 12);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(99, 39);
            this.button6.TabIndex = 8;
            this.button6.Text = "Додати заробітну плату";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBox12);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.textBox7);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.textBox6);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Controls.Add(this.textBox9);
            this.panel2.Controls.Add(this.textBox10);
            this.panel2.Controls.Add(this.textBox11);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.button7);
            this.panel2.Controls.Add(this.textBox5);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.textBox8);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Location = new System.Drawing.Point(674, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(376, 290);
            this.panel2.TabIndex = 9;
            this.panel2.Visible = false;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(178, 41);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(105, 20);
            this.textBox12.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(15, 44);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(158, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Кількість відпрацьованих днів";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(179, 116);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(104, 20);
            this.textBox7.TabIndex = 33;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(289, 15);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(84, 47);
            this.button9.TabIndex = 32;
            this.button9.Text = "Розрахувати заробтну плату";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(179, 242);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(104, 20);
            this.textBox6.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 242);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "Заробітна плата";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016"});
            this.comboBox3.Location = new System.Drawing.Point(179, 89);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(105, 21);
            this.comboBox3.TabIndex = 28;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(179, 218);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(104, 20);
            this.textBox9.TabIndex = 27;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(179, 192);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(104, 20);
            this.textBox10.TabIndex = 26;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(179, 168);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(105, 20);
            this.textBox11.TabIndex = 25;
            this.textBox11.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(15, 168);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "Лікарняні дні";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 218);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "Нарахування на з/п";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 192);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Податки";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(164, 269);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Код нової заробітної плати";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(289, 68);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(84, 21);
            this.button7.TabIndex = 19;
            this.button7.Text = "Додати";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(179, 142);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(104, 20);
            this.textBox5.TabIndex = 18;
            this.textBox5.Text = "0";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBox2.Location = new System.Drawing.Point(179, 65);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(105, 21);
            this.comboBox2.TabIndex = 15;
            this.comboBox2.Visible = false;
            this.comboBox2.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(178, 15);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(105, 20);
            this.textBox8.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(15, 92);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "РІк";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(15, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Місяць";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Розмір премії";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(15, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Код співробітника";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(14, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(119, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Кількість робочих днів";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(289, 91);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(84, 21);
            this.button8.TabIndex = 8;
            this.button8.Text = "Відмінити";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(593, 57);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 63);
            this.button1.TabIndex = 10;
            this.button1.Text = "Видалити останнього внесеного в базу працівника";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(703, 57);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(104, 63);
            this.button10.TabIndex = 11;
            this.button10.Text = "Видалити останню збережену заробітну плату";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 473);
            this.ControlBox = false;
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Автоматизована система обліку заробітної плати";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.db_kursDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableinfBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableinfBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablezpBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKtablezptableinfBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private db_kursDataSet db_kursDataSet;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource tableinfBindingSource;
        private db_kursDataSetTableAdapters.table_infTableAdapter table_infTableAdapter;
        private System.Windows.Forms.BindingSource fKtablezptableinfBindingSource;
        private db_kursDataSetTableAdapters.table_zpTableAdapter table_zpTableAdapter;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource tableinfBindingSource1;
        private System.Windows.Forms.BindingSource tablezpBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn kodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pibDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn posadaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn okladDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kodDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn vidrobdenDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn misyacDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rikDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kodspivrobDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn premDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kilklikDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumapodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn narahuvannyaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zarplataDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button10;
    }
}

