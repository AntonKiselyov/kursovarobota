﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursova
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "db_kursDataSet.table_zp". При необходимости она может быть перемещена или удалена.
            this.table_zpTableAdapter.Fill(this.db_kursDataSet.table_zp);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "db_kursDataSet.table_inf". При необходимости она может быть перемещена или удалена.
            this.table_infTableAdapter.Fill(this.db_kursDataSet.table_inf);
        }
        private void ochist_panel1()
        {
            textBox1.Text = "";
            textBox4.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            comboBox1.Text = "";
            this.panel1.Visible = false;
            this.button3.Visible = true;
            this.button6.Visible = true;
            this.button1.Visible = true;
            this.button10.Visible = true;
            this.Size = new Size(820, 500);
        }
        private void ochist_panel2()
        {
            textBox5.Text = "0";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox11.Text = "0";
            textBox12.Text = "";
            comboBox2.Text = "";
            comboBox3.Text = "";
            this.panel2.Visible = false;
            this.comboBox2.Visible = false;
            this.button7.Visible = false;
            this.button3.Visible = true;
            this.button6.Visible = true;
            this.button1.Visible = true;
            this.button10.Visible = true;
            this.Size = new Size(820, 500);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id = this.dataGridView1.NewRowIndex;
            label7.Text = (id+1).ToString();
            this.panel1.Visible = true;
            this.button3.Visible = false;
            this.button6.Visible = false;
            this.button1.Visible = false;
            this.button10.Visible = false;
            this.Size = new Size(1060,500);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ochist_panel1();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow workRow = db_kursDataSet.table_inf.NewRow();
                workRow[0] = label7.Text;
                workRow[1] = textBox1.Text;
                workRow[2] = textBox3.Text;
                workRow[3] = textBox4.Text;
                workRow[4] = comboBox1.Text;
                workRow[5] = textBox2.Text;
                db_kursDataSet.table_inf.Rows.Add(workRow);
                this.table_infTableAdapter.Update(this.db_kursDataSet);
                MessageBox.Show("Працівника було додано!");
                ochist_panel1();
            }
            catch
            {
                MessageBox.Show("Були заповнені не всі поля!");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int id = this.dataGridView2.NewRowIndex;
            label8.Text = (id+1).ToString();
            this.panel2.Visible = true;
            this.button6.Visible = false;
            this.button3.Visible = false;
            this.button1.Visible = false;
            this.button10.Visible = false;
            this.Size = new Size(1060, 500);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            ochist_panel2();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DataRow workRow = db_kursDataSet.table_zp.NewRow();
            workRow[0] = label8.Text;
            workRow[1] = textBox12.Text;
            workRow[2] = comboBox2.Text;
            workRow[3] = comboBox3.Text;
            workRow[4] = textBox7.Text;
            workRow[5] = textBox5.Text;
            workRow[6] = textBox11.Text;
            workRow[7] = textBox10.Text;
            workRow[8] = textBox9.Text;
            workRow[9] = textBox6.Text;
            db_kursDataSet.table_zp.Rows.Add(workRow);
            this.table_zpTableAdapter.Update(this.db_kursDataSet);
            MessageBox.Show("Заробітну плату було додано!");
            ochist_panel2();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                int id, lik, vidp, rob_dni;
                float oklad, prem, narah, sum_pod, zp;
                id = int.Parse(textBox7.Text) - 1;
                rob_dni = int.Parse(textBox8.Text);
                lik = int.Parse(textBox11.Text);
                vidp = int.Parse(textBox12.Text);
                prem = float.Parse(textBox5.Text);
                oklad = float.Parse(dataGridView1.Rows[id].Cells[5].Value.ToString());
                Zarplata obj = new Zarplata(rob_dni, vidp, lik, prem, oklad);
                sum_pod = obj.sum;
                narah = obj.nar_zp;
                zp = obj.zarp;
                textBox10.Text = sum_pod.ToString();
                textBox9.Text = narah.ToString();
                textBox6.Text = zp.ToString();
                button7.Visible = true;
                MessageBox.Show("Заробітну плату розраховано!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Було введено не достатньо даних для розрахунків!");
            }
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            int x;
            x = int.Parse(comboBox2.Text);
            switch (x)
            {
                case 1:
                    textBox8.Text = "22";
                    break;
                case 3:
                    textBox8.Text = "22";
                    break;
                case 4:
                    textBox8.Text = "22";
                    break;
                case 5:
                    textBox8.Text = "21";
                    break;
                case 6:
                    textBox8.Text = "22";
                    break;
                case 7:
                    textBox8.Text = "23";
                    break;
                case 8:
                    textBox8.Text = "21";
                    break;
                case 9:
                    textBox8.Text = "22";
                    break;
                case 10:
                    textBox8.Text = "22";
                    break;
                case 11:
                    textBox8.Text = "21";
                    break;
                case 12:
                    textBox8.Text = "23";
                    break;
            }
            if (int.Parse(comboBox3.Text) % 4 != 0 && x==2)
            {
                textBox8.Text = "20";
            }
            if (int.Parse(comboBox3.Text) % 4 == 0 && x==2)
            {
                textBox8.Text = "21";                            
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            label11.Visible = true;
            comboBox2.Visible = true;
            comboBox2.Text = "";
            textBox8.Text = "";
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = comboBox1.Text;
            switch (str)
            {
                case "Директор":
                    {
                        textBox2.Text = "10000";
                        break;
                    }
                case "Головний бухгалтер":
                    {
                        textBox2.Text = "8000";
                        break;
                    }
                case "Бухгалтер":
                    {
                        textBox2.Text = "5000";
                        break;
                    }
                case "Менеджер":
                    {
                        textBox2.Text = "4500";
                        break;
                    }
                case "Програміст":
                    {
                        textBox2.Text = "5000";
                        break;
                    }
                case "Юніор":
                    {
                        textBox2.Text = "2000";
                        break;
                    }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = this.dataGridView1.NewRowIndex - 1;
            dataGridView1.Rows.RemoveAt(id);
            this.table_infTableAdapter.Update(this.db_kursDataSet);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            int id = this.dataGridView2.NewRowIndex - 1;
            dataGridView2.Rows.RemoveAt(id);
            this.table_zpTableAdapter.Update(this.db_kursDataSet);
        }
    }
    public class Zarplata
    {
        public float zarp, sum, nar_zp;
        private float esv, pdfo, arm_pod;
        public Zarplata(int rob, int vidp, int lik, float prem, float oklad)
        {
            if (lik > 0)
            {
                float likk = (oklad * 6) / (rob * 6) * lik;
                zarp = oklad / rob * vidp + prem + oklad * 0.12F + likk;
                esv = zarp * 0.036F + likk * 0.02F;
            }
            if(lik == 0)
            {
                zarp = oklad / rob * vidp + prem + oklad * 0.12F;
                esv = zarp * 0.036F;
            }
            pdfo = (zarp - esv) * 0.15F;
            arm_pod = zarp * 0.015F;
            sum = esv + pdfo + arm_pod;
            zarp -= sum;
            nar_zp = zarp * 0.3678F;
        }
    }
}
